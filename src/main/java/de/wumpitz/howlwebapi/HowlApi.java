package de.wumpitz.howlwebapi;

import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

import retrofit.RequestInterceptor;
import retrofit.RestAdapter;
import retrofit.android.MainThreadExecutor;

/**
 * Created by timo on 13.07.15.
 */
public class HowlApi {

    /**
     * Main Howl Web API endpoint
     */
    public static final String HOWL_WEB_API_ENDPOINT = "http://localhost:5000";


    private final HowlService mHowlService;

    /**
     * Create instance of HowlApi with given executors.
     *
     * @param httpExecutor executor for http request. Cannot be null.
     * @param callbackExecutor executor for callbacks. If null is passed than the same
     *                         thread that created the instance is used.
     */
    public HowlApi(Executor httpExecutor, Executor callbackExecutor) {
        mHowlService = init(httpExecutor, callbackExecutor);
    }

    /**
     *  New instance of HowlApi,
     *  with single thread executor both for http and callbacks.
     */
    public HowlApi() {
        Executor httpExecutor = Executors.newSingleThreadExecutor();
        MainThreadExecutor callbackExecutor = new MainThreadExecutor();
        mHowlService = init(httpExecutor, callbackExecutor);
    }

    private HowlService init(Executor httpExecutor, Executor callbackExecutor) {

        final RestAdapter restAdapter = new RestAdapter.Builder()
                .setLogLevel(RestAdapter.LogLevel.BASIC)
                .setExecutors(httpExecutor, callbackExecutor)
                .setEndpoint(HOWL_WEB_API_ENDPOINT)
                //.setRequestInterceptor(new WebApiAuthenticator())
                .build();

        return restAdapter.create(HowlService.class);
    }

    public HowlService getService() {
        return mHowlService;
    }


}
