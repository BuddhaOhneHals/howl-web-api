package de.wumpitz.howlwebapi.models;

public class Binding {
    public String devices;
    public String name;
    public String code;
    public String uri;

    @Override
    public boolean equals(Object other) {
        if (this == other)
            return true;
        if (other == null)
            return false;
        boolean result = false;
        if (other instanceof Binding) {
            Binding that = (Binding) other;
            result = devices != null && devices.equals(that.devices)
                    && name != null && name.equals(that.name)
                    && code != null && code.equals(that.code)
                    && uri != null && uri.equals(that.uri);
        }
        return result;
    }

    @Override
    public int hashCode() {
        int hash = 1;
        hash = hash * 6 + (name == null ? 0 : name.hashCode());
        hash = hash * 10 + (code == null ? 0 : code.hashCode());
        hash = hash * 22 + (uri == null ? 0 : uri.hashCode());
        hash = hash * 24 + (devices == null ? 0 : devices.hashCode());
        return hash;
    }
}