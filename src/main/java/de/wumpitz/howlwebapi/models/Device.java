package de.wumpitz.howlwebapi.models;

import com.google.gson.internal.LinkedTreeMap;

public class Device {

    private String _id;
    public String _type;
    public String name;
    public String uri;
    public LinkedTreeMap fields;

    public String getId() {
        return _id;
    }

    public String getType() {
        return _type;
    }

    @Override
    public boolean equals(Object other) {
        if (this == other)
            return true;
        if (other == null)
            return false;
        boolean result = false;
        if (other instanceof Device) {
            Device that = (Device) other;
            result = getId() != null && getId().equals(that.getId())
                    && getType() != null && getType().equals(that.getType())
                    && name != null && name.equals(that.name)
                    && uri != null && uri.equals(that.uri)
                    && fields != null && fields.equals(that.fields);
        }
        return result;
    }

    @Override
    public int hashCode() {
        int hash = 1;
        hash = hash * 6 + (getId() == null ? 0 : getId().hashCode());
        hash = hash * 10 + (getType() == null ? 0 : getType().hashCode());
        hash = hash * 22 + (name == null ? 0 : name.hashCode());
        hash = hash * 24 + (uri == null ? 0 : uri.hashCode());
        hash = hash * 25 + (fields == null ? 0 : fields.hashCode());
        return hash;
    }
}